<?php

namespace App\Http\Controllers;

class SingleStrategyController extends AbstractJsonRenderer
{
    private $url = "https://participatory-archives.ch/api/items?property[0][property]=1632&property[0][type]=eq&property[0][text]=";


    public function __construct($signature) {
        parent::__construct($signature, $this->url . $signature);
    }

    public function process() {
        [$inCache, $data] = $this->getCached();
        if($inCache) {
            return response()->json($data);
        }
        // do the work
        [$isGood, $omeka_data] = $this->getFromApi();
        if(!$isGood) {
            return response("Error from Omeka API: {$omeka_data}", 500);
        }
        if(count($omeka_data) == 0) {
            return response("No result for from Omeka API for signture" . $this->signature, 500);
        }
        $omeka_data = $omeka_data[0];

        [$isGood, $sipi_data] = $this->getFromSipi($omeka_data["schema:image"][0]['@id'] . '/info.json');
        if(!$isGood) {
            return response("Error from Sipi API: {$sipi_data}", 500);
        }

        $signature = $omeka_data['schema:identifier'][0]['@value'];
        $iiif_url = str_replace('http:', 'https:', $omeka_data['schema:image'][0]['@id']);


        if (isset($omeka_data['schema:name'])) {
            $name = $omeka_data['schema:name'][0]['@value'];
        } else {
            $name = $omeka_data['schema:identifier'][0]['@value'];
        }

        $items = $this->itemBlock($signature, $sipi_data, function () use ($iiif_url, $sipi_data) {
            return [ $this->itemBlock($iiif_url, $sipi_data["height"], $sipi_data["width"]) ];
        });

        $optionalMetaData = $this->optionalMetaData($omeka_data,
                                                    [['schema:material', 'Material'],
                                                    ['schema:artMedium', "Art Medium"],
                                                    ['schema:size', 'Size'],
                                                    ['schema:location', 'Ort'],
                                                    ['schema:copyrightHolder', 'Copyright']]);

        $data = [
            "@context" => "http://iiif.io/api/presentation/3/context.json",
            "id" => ENV('APP_URL') . "/" . $signature . "/manifest.json",
            "type" => "Manifest",

            "label" => [
                "en" => [$name]
            ],

            "metadata" => $this->metadataBlock($name),
            "requiredStatement" => $this->requiredStatementBlock(),
            "provider" => $this->providerBlock(),
            "thumbnail" => $this->thumbnailBlock($iiif_url),
            "viewingDirection" => "left-to-right",
            "items" => $items,
            "seeAlso" => $this->seeAlsoBlock($omeka_data["o:id"])
        ];

        $data["metadata"] = array_merge($data["metadata"], $optionalMetaData);

        $this->save($data);
        return response()->json($data);
    }

}
