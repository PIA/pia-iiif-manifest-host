<?php

namespace App\Http\Controllers;

class DefaultErrorController extends AbstractJsonRenderer
{

    public function __construct($signature) {
        parent::__construct($signature, "bla");
    }

    public function process() {
        return response("I don't know what to do with {$this->signature} ", 500);
    }

    public function save($content) {
        return;
    }
}
