<?php

namespace App\Http\Controllers;


Interface StrategyInterface
{

    public function process();
    public function save($data);
}
