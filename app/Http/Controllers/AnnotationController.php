<?php

namespace App\Http\Controllers;

use App\Models\Detection;
use Illuminate\Support\Facades\Storage;

class AnnotationController extends Controller
{

    public static function jsonStart(string $signature, string $_url)
    {
        return [
            "@context" => "http://iiif.io/api/presentation/3/context.json",
            "id" => $_url . "/annotations/" . $signature . "-p1-list.json",
            "type" => "AnnotationPage",

            "items" => [],
        ];
    }

    public static function jsonBody(string $_url, string $signature, $id, $label, $score, $x, $y, $w, $h)
    {
        return [
            "@context" => "http://www.w3.org/ns/anno.jsonld",
            "id" => $_url . "/annotations/" . $signature . "-p1-list/annotation-" . $id . ".json",
            "motivation" => "commenting",
            "type" => "Annotation",
            "body" => [
                [
                    "type" => "TextualBody",
                    "value" => $label,
                    "purpose" => "commenting"
                ],
                [
                    "type" => "TextualBody",
                    "value" => "Object Detection (vitrivr)",
                    "purpose" => "tagging"
                ],
                [
                    "type" => "TextualBody",
                    "value" => "<br><small>Detection score: " . number_format($score, 4, ".", "") . "</small>",
                    "purpose" => "commenting"
                ],
            ],
            "target" => [
                "source" => $_url . "/" . $signature . "/canvas/p1",
                "selector" => [
                    "type" => "FragmentSelector",
                    "conformsTo" => "http://www.w3.org/TR/media-frags/",
                    "value" => "xywh=" . $x . "," . $y . "," . $w . "," . $h
                ],
                "dcterms:isPartOf" => [
                    "type" => "Manifest",
                    "id" => $_url . "/" . $signature . "/manifest.json"
                ]
            ]
        ];
    }

    public static function fromDatabase(string $_url, string $signature, float $detection_score)
    {
        $data = self::jsonStart($signature, $_url);

        $detections = Detection::where('score', '>=', $detection_score)
            ->where('sgv_signature', $signature)->get();

        foreach ($detections as $key => $detection) {
            $item = self::jsonBody(
                $_url,
                $signature,
                $detection->id,
                $detection->class->label,
                $detection->score,
                $detection->x,
                $detection->y,
                $detection->w,
                $detection->h
            );

            $data['items'][] = $item;
        }
        return $data;
    }

    public static function isValid(string $signature)
    {
        return str_starts_with($signature, "SGV_");
    }

    public static function loadFromApi(string $signature)
    {
        $annotations_url = "https://participatory-archives.ch/api/items?fulltext_search=" . $signature . "&resource_template_id[]=16";
        return json_decode(file_get_contents($annotations_url), true);
    }

    public static function fromApi($_url, string $signature, $annotations)
    {

        $data = self::jsonStart($signature, $_url);

        foreach ($annotations as $key => $annotation) {
            $score = explode(": ", $annotation["oa:hasBody"][1]["@value"])[1];
            $item = self::jsonBody(
                $_url,
                $signature,
                $annotation["o:id"],
                $annotation["oa:hasBody"][0]["@value"],
                $score,
                $annotation["pia:piamedia-frags-x"][0]["@value"],
                $annotation["pia:piamedia-frags-y"][0]["@value"],
                $annotation["pia:piamedia-frags-w"][0]["@value"],
                $annotation["pia:piamedia-frags-h"][0]["@value"],
            );

            $data['items'][] = $item;
        }
        return $data;
    }

    public static function cached($filename)
    {
        return (Storage::disk('public')->exists($filename) &&
            Storage::disk('public')->lastModified($filename) > strtotime('-3 days')
        );
    }
}
