<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Http;

class AlbumStrategyController extends AbstractJsonRenderer
{
    private $url = "https://participatory-archives.ch/api/items?property[0][joiner]=and&property[0][property]=957&property[0][type]=eq&property[0][text]=";
    private $parentSignature;

    public function __construct($signature) {
        $ex = explode("_", $signature);
        $this->parentSignature = "{$ex[0]}_{$ex[1]}_{$ex[2]}";
        parent::__construct($signature, $this->url . $this->parentSignature);
    }

    public function process() {
        [$inCache, $data] = $this->getCached();
        if($inCache) {
            return response()->json($data);
        }
        // do the work
        [$isGood, $omeka_data] = $this->getFromApi();
        if(!$isGood) {
            return response("Error from Omeka API: {$omeka_data}", 500);
        }
        $omeka_data = $omeka_data[0];

        $pages = collect($omeka_data["schema:hasPart"])
            ->map(function($value) { return $this->getPage($value["@id"]);})
            ->filter(function($value) { return $value[0]; })
            ->map(function($value) { return $value[1]; });

        $pages = $pages->sortBy("o:title")->values();

        $len_pages = $pages->count();

        $signature = $omeka_data['schema:identifier'][0]['@value'];


        if (isset($omeka_data['schema:name'])) {
            $name = $omeka_data['schema:name'][0]['@value'];
        } else {
            $name = $omeka_data['schema:identifier'][0]['@value'];
        }

        $canvases = collect([]);
        for ($i = 0; $i < $len_pages; $i++) {
            $canvases->push($this->canvasRender($pages[$i], $i));
        }
        $iiif_url = $canvases->first()["items"][0]["id"];

        $optionalMetaData = $this->optionalMetaData($omeka_data,
                                                    [['schema:material', 'Material'],
                                                    ['schema:artMedium', "Art Medium"],
                                                    ['schema:size', 'Size'],
                                                    ['schema:location', 'Ort'],
                                                    ['schema:copyrightHolder', 'Copyright']]);

        $data = [
            "@context" => "http://iiif.io/api/presentation/3/context.json",
            "id" => ENV('APP_URL') . "/" . $signature . "/manifest.json",
            "type" => "Manifest",

            "label" => [
                "en" => [$name]
            ],

            "metadata" => $this->metadataBlock($name),
            "requiredStatement" => $this->requiredStatementBlock(),
            "provider" => [$this->providerBlock()],
            "thumbnail" => $this->thumbnailBlock($iiif_url),
            "viewingDirection" => "left-to-right",
            "items" => $canvases,
            "annotations" => $this->annotationLinkBlock($signature),
            "seeAlso" => $this->seeAlsoBlock($omeka_data["o:id"])
        ];

        $data["metadata"] = array_merge($data["metadata"], $optionalMetaData);

        $this->save($data);
        return response()->json($data);
    }

    private function getPage($id) {
        $response = Http::get($id);
        if($response->successful()) {
            return [true, $response->json()];
        }
        else {
            return [false, $response->status()];
        }
    }

    private function canvasRender($item, int $index) {
        $signature = $item["o:title"];
        $items = collect([]);
        // load own image at schema:isPartOf
        [$is_okay, $page_image] = $this->getPage($item["schema:isPartOf"][0]["@id"]);
        if ($is_okay) {
            $items->push($page_image);
        }
        $item = collect($item);
        // load all items at schema:hasPart
        $child_items = collect($item->get("schema:hasPart", []))
            ->map( function ($i) { return $this->getPage($i["@id"]); })
            ->filter( function($x) { return $x[0]; })
            ->map( function ($i) { return $i[1]; } );

        $items = $items->merge($child_items);

        $sipi_items = $items
            ->map( function($i) { return $this->getFromSipi($i["schema:image"][0]["@id"]); } )
            ->filter( function($x) { return $x[0]; } )
            ->map( function($i) {
                return $i[1];
            });

        $itemBlocks = $sipi_items->map(function ($data) {
                return $this->itemBlock($data["id"], $data["height"], $data["width"]);
        });

        $annotations = $items->map(function ($data) {
           return $this->annotationLinkBlock($data["schema:identifier"][0]["@value"]);
        });

        // each item needs this block
        $a = $this->annotationBlock($signature, $index, 1, $itemBlocks);
        $ap = $this->annotationPageBlock($signature, $index, 1, [$a]);

        $hw = [
            "height" => $sipi_items->max( function ($i) { return $i["height"]; }),
            "width" => $sipi_items->max( function($i) { return $i["width"]; })
        ];

        $c = $this->canvasBlock($signature, $hw, $index, [$ap], $annotations);

        return $c;
    }
}
