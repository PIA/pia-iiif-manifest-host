<?php

namespace App\Http\Controllers;

use GuzzleHttp\Exception\ConnectException;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;

abstract class AbstractJsonRenderer extends Controller implements StrategyInterface
{

    protected string $apiUrl;
    protected string $signature;

    function __construct(string $signature, string $url) {
        $this->signature = $signature;
        $this->apiUrl = $url;
    }

    public abstract function process();

    private function isCached(string $filename) {
        return (App::environment() != "local"
                && Storage::disk('public')->exists($filename)
                && Storage::disk('public')->lastModified($filename) > strtotime('-3 days')
        );
    }

    private function filename() {
        return "{$this->signature}.json";
    }

    protected function getCached() {
        $f = $this->filename();
        if($this->isCached($f)) {
            return [ true, json_decode(Storage::disk('public')->get($f), true)];
        }
        return [ false, null ];
    }

    public function save($data) {
        Storage::disk('public')->put($this->filename(), json_encode($data));
    }

    protected function getFromApi() {
        $response = Http::get($this->apiUrl);
        if($response->successful()) {
            return [true, $response->json()];
        }
        else {
            return [false, $response->status()];
        }
    }

    protected function getFromSipi(string $url) {
        $url = str_replace('https:', 'http:', $url);
        try {
            $response = Http::get($url);
        }
        catch (ConnectException $e) {
            return [false, $e->getMessage()];
        }
        if($response->successful()) {
            $json = $response->json();
            $json["id"] = str_replace("http", "https", $json["id"]);
            return [true, $json];
        }
        else {
            return [false, $response->status()];
        }
    }

    protected function metadataBlock($name) {
            return [
                [
                    "label" => $this->labelValueBlock(["Titel"]),
                    "value" => $this->labelValueBlock([$name])
                ]
            ];
    }

    protected function requiredStatementBlock() {
        return [
                "label" => $this->labelValueBlock(["Copyright"]),
                "value" => $this->labelValueBlock(["Schweizerische Gesellschaft für Volkskunde (SGV)",],
                                                  ["Socité Suisse des Traditions populaires (SSTP)",],
                                                  ["Società Svizzera per le Tradizioni Popolari (SSTP)",],
                                                  ["Swiss Society for Folklore Studies (SSFS)",])
            ];
    }

    protected function serviceBlock(string $id) {
        return [
            [
                "id" => $id,
                "type" => "ImageService3",
                "profile" => "level2",
            ],
        ];
    }

    protected function annotationBlock($signature, $canvasIndex, $annotationIndex, $items) {
        $aIString = sprintf('%04d', $annotationIndex);
        $cIString = $this->formatCanvasIndex($canvasIndex);
        return [
            "id" =>
            "https://iiif.participatory-archives.ch/" . $signature . "/annotation/p{$aIString}-image",
            "type" => "Annotation",
            "motivation" => "painting",
            "body" => $items,
            "target" =>
            "https://iiif.participatory-archives.ch/" . $signature . "/canvas/p{$cIString}",
        ];
    }

    protected function annotationPageBlock($signature, $canvasIndex, $annotatationPageIndex, $items) {
        $cIString = $this->formatCanvasIndex($canvasIndex);
        return
        [
            "id" =>
            "https://iiif.participatory-archives.ch/" . $signature . "/canvas/p{$cIString}/{$annotatationPageIndex}",
            "type" => "AnnotationPage",
            "items" => $items
        ];
    }

    protected function itemBlock($iiif_url, $height, $width) {
        return [
            "id" => $this->fullMaxUrl($iiif_url),
            "type" => "Image",
            "format" => "image/jpeg",
            "height" => $height,
            "width" => $width,
            "service" => $this->serviceBlock($iiif_url),
            ];
    }

    protected function providerBlock() {
        return [
            "id" => "https://d-nb.info/gnd/1186091584",
            "type" => "Agent",
            "label" => $this->labelValueBlock(["Schweizerische Gesellschaft für Volkskunde. Fotoarchiv",]),
            "logo" => [$this->logoBlock()],
            "homepage" => [
                [
                    "id" => "https://archiv.sgv-sstp.ch/",
                    "type" => "Text",
                    "label" => [
                        "en" => ["SSFS Photo archives"],
                        "de" => ["SGV Fotoarchiv"],
                        "fr" => ["Archives de la SSTP"],
                    ],
                    "format" => "text/html",
                    "language" => ["de"],
                ],
            ]
        ];
    }

    protected function logoBlock() {
        return $this->itemBlock("https://sipi.participatory-archives.ch/SGV_logo.jp2", 149, 500);
    }

    protected function fullMaxUrl($url) {
        return $this->sipiUrl($url, "full", "max", "0");
    }

    protected function sipiUrl($url, $region, $size, $rotation, $format = "jpg", $quality = "default" ) {
        return "{$url}/{$region}/{$size}/{$rotation}/{$quality}.{$format}";
    }


    protected function labelValueBlock($de = null, $fr = null, $it = null, $en = null) {
        $data = [];
        if(!is_null($de)) $data["de"] = $de;
        if(!is_null($fr)) $data["fr"] = $fr;
        if(!is_null($it)) $data["it"] = $it;
        if(!is_null($en)) $data["en"] = $en;
        return $data;
    }

    protected function thumbnailBlock($url) {
        return  [
            [
                "id" => $this->sipiUrl($url, "full", "80,", "0", "jpg"),
                "type" => "Image",
                "format" => "image/jpeg",
                "service" => [
                    [
                        "id" => $url,
                        "type" => "ImageService3",
                        "profile" => "level2",
                    ],
                ],
            ]
        ];
    }

    protected function canvasBlock($sig, $sipi_data, $canvasIndex, $ap, $annotations = []) {
        $cid = $this->formatCanvasIndex($canvasIndex);
        return
        [

            "id" => "https://iiif.participatory-archives.ch/" . $sig . "/canvas/p{$cid}",
            "type" => "Canvas",
            "label" => [
                "none" => [$sig],
            ],
            "height" => $sipi_data['height'],
            "width" => $sipi_data['width'],
            "items" => $ap,
            "annotations" => $annotations
        ];
    }

    protected function seeAlsoBlock($oid) {
        return  [
            [
                "id" => "https://participatory-archives.ch/api/items/" . $oid,
                "type" => "Dataset",
                "label" => [
                    "en" => [
                        "PIA JSON-LD API"
                    ]
                ],
                "format" => "application/ld+json"
            ],
        ];
    }

    protected function formatCanvasIndex($canvasIndex) {
        return sprintf('%01d', $canvasIndex);
    }

    protected function annotationLinkBlock($sig) {
        return
                [
                    "id" => ENV('APP_URL') . "/annotations/" . $sig . "-p1-list.json",
                    "type" => "AnnotationPage",
                ];
    }

    protected function optionalMetaData($omeka_data, $inputs) {
        $output = [];
        foreach ($inputs as [$key, $label]) {
            if (isset($omeka_data[$key])) {
                $value = ( isset($omeka_data[$key][0]['display_title']) ? $omeka_data[$key][0]['display_title'] : $omeka_data[$key][0]['@value']);
                $output[] = [
                    "label" => [
                        "de" => [$label]
                    ],
                    "value" => [
                        "de" => [$value]
                    ]
                ];
            }
        }
        return $output;
    }
}
