<?php

namespace App\Http\Controllers;

use Illuminate\Http\Client\ConnectionException;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Http;

class AlbumController extends Controller
{
    public static function loadFromApi(string $sig) {
        $image_url = "https://participatory-archives.ch/api/items?property[0][property]=1632&property[0][type]=eq&property[0][text]=" . $sig;
        // if(self::cachedItem($image_url)) {
        //     return json_decode(Storage::disk('public')->get($image_url), true);
        // }
        $response = Http::get($image_url);
        if($response->successful()) {
            return $response->json()[0];
        }
        else {
            throw new ConnectionException();
        }
        // Storage::disk("public")->put($image_url, json_encode($json));
    }

    public static function loadUrlFromApi(string $url) {
        if(self::cachedItem($url)) {
            return json_decode(Storage::disk('public')->get($url), true);
        }
        $json = json_decode(file_get_contents($url), true);
        Storage::disk("public")->put($url, json_encode($json));
        return $json;
    }

    public static function loadIiifJson(string $id) {

        $url = $id .'/info.json';
        //if(self::cachedIiif($url)) {
            //return json_decode(Storage::disk('public')->get($url), true);
        //}
        $response = Http::get($url);
        if($response->successful()) {
            return $response->json();
        }
        else {
            throw new ConnectionException();
        }
        //Storage::disk("public")->put($url, json_encode($info_json));
    }

    public static function cachedItem($filename)
    {
        return (Storage::disk('public')->exists($filename) &&
            Storage::disk('public')->lastModified($filename) > strtotime('-3 days')
        );
    }

    public static function cachedIiif($filename) {
        return (Storage::disk('public')->exists($filename) &&
            Storage::disk('public')->lastModified($filename) > strtotime('-10 days')
        );
    }

    public static function makeBook($book, $sig) {

        $return_book = [
            "@context" => "http://iiif.io/api/presentation/3/context.json",
            "id" => ENV('APP_URL') . "/" . $sig . "/manifest.json",
            "type" => "Manifest",
            "label" => [
                "en" => [$book["o:title"]]
            ],
            "requiredStatement" => [
                "label" => [
                    "de" => ["Copyright"],
                ],
                "value" => [
                    "de" => [
                        "Schweizerische Gesellschaft für Volkskunde (SGV)",
                    ],
                    "fr" => [
                        "Socité Suisse des Traditions populaires (SSTP)",
                    ],
                    "it" => [
                        "Società Svizzera per le Tradizioni Popolari (SSTP)",
                    ],
                    "en" => [
                        "Swiss Society for Folklore Studies (SSFS)",
                    ],
                ],
            ],
            "provider" => [
                [
                    "id" => "https://d-nb.info/gnd/1186091584",
                    "type" => "Agent",
                    "label" => [
                        "de" => [
                            "Schweizerische Gesellschaft für Volkskunde. Fotoarchiv",
                        ],
                    ],
                    "logo" => [
                        [
                            "id" =>
                            "https://sipi.participatory-archives.ch/SGV_logo.jp2/full/max/0/default.jpg",
                            "type" => "Image",
                            "format" => "image/jpeg",
                            "height" => 149,
                            "width" => 500,
                            "service" => [
                                [
                                    "id" => "https://sipi.participatory-archives.ch/SGV_logo.jp2",
                                    "type" => "ImageService3",
                                    "profile" => "level2",
                                ],
                            ],
                        ],
                    ],
                    "homepage" => [
                        [
                            "id" => "https://archiv.sgv-sstp.ch/",
                            "type" => "Text",
                            "label" => [
                                "en" => ["SSFS Photo archives"],
                                "de" => ["SGV Fotoarchiv"],
                                "fr" => ["Archives de la SSTP"],
                            ],
                            "format" => "text/html",
                            "language" => ["de"],
                        ],
                    ],
                ],
            ],
            "thumbnail" => [
            ],
            "viewingDirection" => "left-to-right",
            "seeAlso" => [
                [
                    "id" => $book["@id"],
                    "type" => "Dataset",
                    "label" => [
                        "en" => [
                            "PIA JSON-LD API"
                        ]
                    ],
                    "format" => "application/ld+json"
                ],
            ],
        ];

        $items = collect($book["schema:hasPart"])->slice(0, 1)->map(function($layer) { return self::makeLayer($layer); });

        $return_book["items"] = $items;

        return $return_book;
    }

    // This is a iiif canvas
    public static function makeLayer($layer) {
        // create outer object
        // for each item create object
        // insert layer image into items
        // return object to insert into parent
        $return_layer = [
            "id" => $layer["@id"],
            "type" => "Canvas",
            "label" => [ "en" => $layer["display_title"] ],
            "height" => "extract max height from items",
            "width" => "extract max width from items",
            "items" => []
        ];

        // add layer item to items
        $data = self::loadUrlFromApi($layer["@id"]);

        $items = collect([]);
        if(isset($data["schema:isPartOf"])) {
            $item = self::loadUrlFromApi($data["schema:isPartOf"][0]["@id"]);
            $items->push($item);
        }

        if(isset($data["schema:hasPart"])) {
            $children = collect($data["schema:hasPart"])->map(function($c) {
                return self::loadUrlFromApi($c["@id"]);
            });
            $items->merge($children);
        }

        dump($items->all());

        $sipi_items = $items->map(function ($i) {
            $images = collect($i["schema:image"])->map(function ($image) {
                return self::loadIiifJson($image["@id"]);
            });
        });

        dump($sipi_items->all());

        return $return_layer;
    }

    // This is a iiif canvas item
    public static function makeLayerItem($item) {
        // create object
        // add all the children as items
        // return object to parent
    }
}
