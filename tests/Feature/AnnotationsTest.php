<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\AnnotationController;
use Illuminate\Support\Facades\File;

use function PHPUnit\Framework\assertEquals;
use function PHPUnit\Framework\assertEqualsCanonicalizing;
use function PHPUnit\Framework\assertFalse;
use function PHPUnit\Framework\assertTrue;

class AnnotationsTest extends TestCase
{

    public function test_cached()
    {
        Storage::disk('public')->put("abc", "1");
        $check = AnnotationController::cached("abc");
        assertTrue($check);
    }

    public function test_old()
    {
        $content = File::get(base_path("tests/Feature/SGV_12N_38284-p1-list.json"));
        $json = json_decode($content, true);
        $a = AnnotationController::fromDatabase("https://iiif.participatory-archives.ch", "SGV_12N_38284", 0.75);

        assertEquals($json, $a);
    }

    public function test_same_annotation()
    {
        $annotations = json_decode(File::get(base_path("tests/Feature/SGV_12N_38284-annotations.json")), true);
        $a = AnnotationController::fromDatabase("https://iiif.participatory-archives.ch", "SGV_12N_38284", 0.5);
        $b = AnnotationController::fromApi("https://iiif.participatory-archives.ch", "SGV_12N_38284", $annotations);

        // remove ids from json because they are not stable
        unset($a["items"][0]["id"]);
        unset($a["items"][1]["id"]);
        unset($a["items"][2]["id"]);
        unset($a["items"][3]["id"]);
        unset($a["items"][4]["id"]);
        unset($a["items"][5]["id"]);


        unset($b["items"][0]["id"]);
        unset($b["items"][1]["id"]);
        unset($b["items"][2]["id"]);
        unset($b["items"][3]["id"]);
        unset($b["items"][4]["id"]);
        unset($b["items"][5]["id"]);

        assertEqualsCanonicalizing($a, $b);
    }

    public function test_signature_check()
    {
        assertTrue(AnnotationController::isValid("SGV_12N_38284"));
        assertFalse(AnnotationController::isValid("ABC"));
    }
}
