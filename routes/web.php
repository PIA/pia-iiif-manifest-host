<?php

use App\Http\Controllers\AlbumStrategyController;
use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

use App\Models\Detection;
use App\Http\Controllers\AnnotationController;
use App\Http\Controllers\DefaultErrorController;
use App\Http\Controllers\SingleStrategyController;

Route::get('/', function () {
    return response("nothing to see here", 400);
});

Route::get('/health', function () {
   return response("all good");
});

Route::get('/{signature}/manifest.json', function (Request $request, $signature) {

    $filename = $signature . '.json';

    if (false && AnnotationController::cached($filename)) {
        return response()->json(json_decode(Storage::disk('public')->get($filename)));
    } else {

        // extract SGV_12N
        $type_prefix = substr($signature, 6, 1);


        $strategy = match($type_prefix) {
            "P" => new SingleStrategyController($signature),
            "N" => new SingleStrategyController($signature),
            "D" => new SingleStrategyController($signature),
            "A" => new AlbumStrategyController($signature),
            default => new DefaultErrorController($signature)
        };

        return $strategy->process();


    $image_url = "https://participatory-archives.ch/api/items?property[0][property]=1632&property[0][type]=eq&property[0][text]=" . $signature;
        $image = json_decode(file_get_contents($image_url), true)[0];

        $url = $image['schema:image'][0]['@id'] . '/info.json';
        $info_json = json_decode(file_get_contents(str_replace('https:', 'http:', $url)), true);

        $signature = $image['schema:identifier'][0]['@value'];
        $iiif_url = str_replace('http:', 'https:', $image['schema:image'][0]['@id']);

        if (isset($image['schema:name'])) {
            $name = $image['schema:name'][0]['@value'];
        } else {
            $name = $image['schema:identifier'][0]['@value'];
        }

        $data = [
            "@context" => "http://iiif.io/api/presentation/3/context.json",
            "id" => ENV('APP_URL') . "/" . $signature . "/manifest.json",
            "type" => "Manifest",

            "label" => [
                "en" => [$name]
            ],

            "metadata" => [
                [
                    "label" => [
                        "de" => ["Titel"]
                    ],
                    "value" => [
                        "de" => [$name]
                    ]
                ]
            ],

            "requiredStatement" => [
                "label" => [
                    "de" => ["Copyright"],
                ],
                "value" => [
                    "de" => [
                        "Schweizerische Gesellschaft für Volkskunde (SGV)",
                    ],
                    "fr" => [
                        "Socité Suisse des Traditions populaires (SSTP)",
                    ],
                    "it" => [
                        "Società Svizzera per le Tradizioni Popolari (SSTP)",
                    ],
                    "en" => [
                        "Swiss Society for Folklore Studies (SSFS)",
                    ],
                ],
            ],
            "provider" => [
                [
                    "id" => "https://d-nb.info/gnd/1186091584",
                    "type" => "Agent",
                    "label" => [
                        "de" => [
                            "Schweizerische Gesellschaft für Volkskunde. Fotoarchiv",
                        ],
                    ],
                    "logo" => [
                        [
                            "id" =>
                            "https://sipi.participatory-archives.ch/SGV_logo.jp2/full/max/0/default.jpg",
                            "type" => "Image",
                            "format" => "image/jpeg",
                            "height" => 149,
                            "width" => 500,
                            "service" => [
                                [
                                    "id" => "https://sipi.participatory-archives.ch/SGV_logo.jp2",
                                    "type" => "ImageService3",
                                    "profile" => "level2",
                                ],
                            ],
                        ],
                    ],
                    "homepage" => [
                        [
                            "id" => "https://archiv.sgv-sstp.ch/",
                            "type" => "Text",
                            "label" => [
                                "en" => ["SSFS Photo archives"],
                                "de" => ["SGV Fotoarchiv"],
                                "fr" => ["Archives de la SSTP"],
                            ],
                            "format" => "text/html",
                            "language" => ["de"],
                        ],
                    ],
                ],
            ],
            "thumbnail" => [
                [
                    "id" => $iiif_url . "/full/80,/0/default.jpg",
                    "type" => "Image",
                    "format" => "image/jpeg",
                    "service" => [
                        [
                            "id" => $iiif_url,
                            "type" => "ImageService3",
                            "profile" => "level2",
                        ],
                    ],
                ],
            ],
            "viewingDirection" => "left-to-right",
            /**
             * TODO
             * "navDate" => "1937-01-01T00:00:00+00:00",
             */
            "items" => [
                [
                    "id" => "https://iiif.participatory-archives.ch/" . $signature . "/canvas/p1",
                    "type" => "Canvas",
                    "label" => [
                        "none" => [$signature],
                    ],
                    "height" => $info_json['height'],
                    "width" => $info_json['width'],
                    "items" => [
                        [
                            "id" =>
                            "https://iiif.participatory-archives.ch/" . $signature . "/canvas/p1/1",
                            "type" => "AnnotationPage",
                            "items" => [
                                [
                                    "id" =>
                                    "https://iiif.participatory-archives.ch/" . $signature . "/annotation/p0001-image",
                                    "type" => "Annotation",
                                    "motivation" => "painting",
                                    "body" => [
                                        "id" => $iiif_url . "/full/max/0/default.jpg",
                                        "type" => "Image",
                                        "format" => "image/jpeg",
                                        "height" => $info_json['height'],
                                        "width" => $info_json['width'],
                                        "service" => [
                                            [
                                                "id" => $iiif_url,
                                                "type" => "ImageService3",
                                                "profile" => "level2",
                                            ],
                                        ],
                                    ],
                                    "target" =>
                                    "https://iiif.participatory-archives.ch/" . $signature . "/canvas/p1",
                                ],
                            ],
                        ],
                    ],
                ],
            ],

            "seeAlso" => [
                [
                    "id" => "https://participatory-archives.ch/api/items/" . $image['o:id'],
                    "type" => "Dataset",
                    "label" => [
                        "en" => [
                            "PIA JSON-LD API"
                        ]
                    ],
                    "format" => "application/ld+json"
                ],
            ],

        ];

        if (isset($image['schema:material'])) {
            $data["metadata"][] = [
                "label" => [
                    "de" => ["Material"]
                ],
                "value" => [
                    "de" => [$image['schema:material'][0]['display_title']]
                ]
            ];
        }

        if (isset($image['schema:artMedium'])) {
            $data["metadata"][] = [
                "label" => [
                    "de" => ["Art Medium"]
                ],
                "value" => [
                    "de" => [$image['schema:artMedium'][0]['display_title']]
                ]
            ];
        }

        if (isset($image['schema:size'])) {
            $data["metadata"][] = [
                "label" => [
                    "de" => ["Size"]
                ],
                "value" => [
                    "de" => [$image['schema:size'][0]['display_title']]
                ]
            ];
        }

        if (isset($image['schema:location'])) {
            $data["metadata"][] = [
                "label" => [
                    "de" => ["Ort"]
                ],
                "value" => [
                    "de" => [$image['schema:location'][0]['display_title']]
                ]
            ];
        }

        if (isset($image['schema:temporal'])) {

            $data["metadata"][] = [
                "label" => [
                    "de" => ["Datum"]
                ],
                "value" => [
                    "de" => [$image['schema:temporal'][0]['@value']]
                ]
            ];
        }

        if (isset($image['schema:copyrightHolder'])) {
            $data["metadata"][] = [
                "label" => [
                    "de" => ["Copyright"]
                ],
                "value" => [
                    "de" => [$image['schema:location'][0]['display_title']]
                ]
            ];
        }

        /* $detection_score = 0.75;
        $detection_score_string = '';

        if ($request->score) {
            $detection_score = $request->score;
            $detection_score_string = '?score=' . $detection_score;
        }

        if ($image->detections->where('score', '>=', $detection_score)->count() > 0) {
            $data['items'][0]['annotations'] = [
                "id" => ENV('APP_URL') . "/annotations/" . $signature . "-p1-list.json" . $detection_score_string,
                "type" => "AnnotationPage",
                "target" => ENV('APP_URL') . "/" . $signature . "/canvas/p1"
            ];
        } */

        $detection_score = 0.75;
        $detection_score_string = '';

        if ($request->score) {
            $detection_score = $request->score;
            $detection_score_string = '?score=' . $detection_score;
        }

        $detections = Detection::where('score', '>=', $detection_score)
            ->where('sgv_signature', $signature)->get();

        if ($detections->count() > 0) {
            $data['items'][0]['annotations'] = [
                [
                    "id" => ENV('APP_URL') . "/annotations/" . $signature . "-p1-list.json" . $detection_score_string,
                    "type" => "AnnotationPage",
                ]
            ];
        }

        Storage::disk('public')->put($filename, json_encode($data));

        return response()->json($data);
    }
});


Route::get('/collections/{id}.json', function ($id) {

    $filename = 'collection_' . $id . '.json';

    if (AnnotationController::cached($filename)) {
        return response()->json(json_decode(Storage::disk('public')->get($filename)));
    } else {

        $collection_url = "https://participatory-archives.ch/api/item_sets/" . $id;
        $collection = json_decode(file_get_contents($collection_url), true);

        $collection_images_url = "https://participatory-archives.ch/api/items?resource_class_id[]=26&item_set_id[]=" . $id;
        $collection_images = json_decode(file_get_contents($collection_images_url), true);

        $data = [
            "@context" => "http://iiif.io/api/presentation/3/context.json",
            "id" => ENV('APP_URL') . "/collections/" . $id . ".json",
            "type" => "Collection",

            "label" => [
                "en" => [$collection['o:title']]
            ],
            "provider" => [
                [
                    "id" => "https://d-nb.info/gnd/1186091584",
                    "type" => "Agent",
                    "label" => [
                        "de" => [
                            "Schweizerische Gesellschaft für Volkskunde. Fotoarchiv",
                        ],
                    ],
                    "homepage" => [
                        [
                            "id" => "https://archiv.sgv-sstp.ch/",
                            "type" => "Text",
                            "label" => [
                                "en" => ["SSFS Photo archives"],
                                "de" => ["SGV Fotoarchiv"],
                                "fr" => ["Archives de la SSTP"],
                            ],
                            "format" => "text/html",
                            "language" => ["de"],
                        ],
                    ],
                ],
                "logo" => [
                    [
                        "id" =>
                        "https://sipi.participatory-archives.ch/SGV_logo.jp2/full/max/0/default.jpg",
                        "type" => "Image",
                        "format" => "image/jpeg",
                        "height" => 149,
                        "width" => 500,
                        "service" => [
                            [
                                "id" => "https://sipi.participatory-archives.ch/SGV_logo.jp2",
                                "type" => "ImageService3",
                                "profile" => "level2",
                            ],
                        ],
                    ],
                ],
            ],
            "seeAlso" => [
                [
                    "id" => "https://participatory-archives.ch/api/item_sets/" . $id,
                    "type" => "Dataset",
                    "label" => [
                        "en" => [
                            "PIA JSON-LD API"
                        ]
                    ],
                    "format" => "application/ld+json"
                ],
            ],
        ];

        if (count($collection_images) > 0) {
            $data["items"] = [];
        }

        foreach ($collection_images as $key => $image) {
            $image_data = [
                "id" => ENV('APP_URL') . "/" . $image['schema:identifier'][0]['@value'] . "/manifest.json",
                "type" => "Manifest",
            ];

            if (isset($image['schema:name'])) {
                $image_data["label"] = [
                    "en" => [$image['schema:name'][0]['@value']]
                ];
            } else {
                $image_data["label"] = [
                    "en" => [$image['schema:identifier'][0]['@value']]
                ];
            }

            $data["items"][] = $image_data;
        }

        Storage::disk('public')->put($filename, json_encode($data));

        return response()->json($data);
    }
})->where('id', '[0-9]+');

Route::get('/collections/toplevel.json', function () {

    $filename = 'toplevel.json';

    if (AnnotationController::cached($filename)) {
        return response()->json(json_decode(Storage::disk('public')->get($filename)));
    } else {

        $collections_url = "https://participatory-archives.ch/api/item_sets?is_open=1";
        $collections = json_decode(file_get_contents($collections_url), true);

        $data = [
            "@context" => "http://iiif.io/api/presentation/3/context.json",
            "id" => ENV('APP_URL') . "/collections/toplevel.json",
            "type" => "Collection",

            "label" => [
                "en" => "IIIF-top-level Collection from the SSFS Photo Archives"
            ],
            "provider" => [
                [
                    "id" => "https://d-nb.info/gnd/1186091584",
                    "type" => "Agent",
                    "label" => [
                        "de" => [
                            "Schweizerische Gesellschaft für Volkskunde. Fotoarchiv",
                        ],
                    ],
                    "homepage" => [
                        [
                            "id" => "https://archiv.sgv-sstp.ch/",
                            "type" => "Text",
                            "label" => [
                                "en" => ["SSFS Photo archives"],
                                "de" => ["SGV Fotoarchiv"],
                                "fr" => ["Archives de la SSTP"],
                            ],
                            "format" => "text/html",
                            "language" => ["de"],
                        ],
                    ],
                ],
            ],
            "logo" => [
                [
                    "id" =>
                    "https://sipi.participatory-archives.ch/SGV_logo.jp2/full/max/0/default.jpg",
                    "type" => "Image",
                    "format" => "image/jpeg",
                    "height" => 149,
                    "width" => 500,
                    "service" => [
                        [
                            "id" => "https://sipi.participatory-archives.ch/SGV_logo.jp2",
                            "type" => "ImageService3",
                            "profile" => "level2",
                        ],
                    ],
                ],
            ]
        ];

        if (count($collections) > 0) {
            $data["items"] = [];
        }

        foreach ($collections as $key => $collection) {
            $data["items"][] = [
                "id" => ENV('APP_URL') . "/collections/" . $collection['o:id'] . ".json",
                "type" => "Collection",

                "label" => [
                    "en" => [$collection['o:title']]
                ]
            ];
        }

        Storage::disk('public')->put($filename, json_encode($data));

        return response()->json($data);
    }
});

Route::get('/annotations/{signature}-p1-list.json', function (Request $request, $signature) {
    if (AnnotationController::isValid($signature)) {
        $filename = 'annotations_' . $signature . '.json';

        if (AnnotationController::cached($filename)) {
            return response()->json(json_decode(Storage::disk('public')->get($filename)));
        } else {

            $annotation = AnnotationController::loadFromApi($signature);
            $data = AnnotationController::fromApi(ENV("APP_URL"), $signature, $annotation);

            Storage::disk('public')->put($filename, json_encode($data));

            return response()->json($data);
        }
    }
    abort(404);
});

Route::get('/activity/all-changes.json', function (Request $request) {

    $filename = 'all_changes.json';

    if (AnnotationController::cached($filename)) {
        return response()->json(json_decode(Storage::disk('public')->get($filename)));
    } else {

        $items_url = "https://participatory-archives.ch/api/items?resource_template_id[]=9";
        file_get_contents($items_url);

        $links_raw = explode(',', $http_response_header[3]);
        $links = [];

        foreach ($links_raw as $key => $link) {
            $link = explode(';', $link)[0];
            $link = str_replace(['<', '>'], '', $link);
            $links[] = trim($link);
        }

        $total_pages = explode('page=', $links[2])[1];
        $total_items = explode(': ', $http_response_header[4])[1];

        $data = [
            "@context" => "http://iiif.io/api/discovery/1/context.json",
            "id" => ENV('APP_URL') . "/activity/all-changes.json",
            "type" => "OrderedCollection",
            "totalItems" => intval($total_items),
            "first" => [
                "id" => ENV('APP_URL') . "/activity/page-0.json",
                "type" => "OrderedCollectionPage"
            ],
            "last" => [
                "id" => ENV('APP_URL') . "/activity/page-" . ($total_pages - 1) . ".json",
                "type" => "OrderedCollectionPage"
            ]
        ];

        Storage::disk('public')->put($filename, json_encode($data));

        return response()->json($data);
    }
});

Route::get('/activity/page-{page}.json', function (Request $request, $page) {

    $filename = 'changes_page-' . $page . '.json';

    if (AnnotationController::cached($filename)) {
        return response()->json(json_decode(Storage::disk('public')->get($filename)));
    } else {

        $actual_page = $page + 1;

        $items_url = "https://participatory-archives.ch/api/items?resource_template_id[]=9&page=" . $actual_page;
        file_get_contents($items_url);

        $items = json_decode(file_get_contents($items_url), true);

        $links_raw = explode(',', $http_response_header[3]);
        $links = [];

        foreach ($links_raw as $key => $link) {
            $link = explode(';', $link)[0];
            $link = str_replace(['<', '>'], '', $link);
            $links[] = trim($link);
        }

        $total_pages = explode('page=', $links[2])[1];

        $data = [
            "@context" => "http://iiif.io/api/discovery/1/context.json",
            "id" => ENV('APP_URL') . "/activity/page-" . $page . ".json",
            "type" => "OrderedCollectionPage",
            "startIndex" => $page * 25 + 1,
            "partOf" => [
                "id" => ENV('APP_URL') . "/activity/all-changes.json",
                "type" => "OrderedCollection"
            ],
            "orderedItems" => []
        ];

        foreach ($items as $key => $item) {
            $signature = $item['schema:identifier'][0]['@value'];

            if (isset($item['schema:name'])) {
                $name = $item['schema:name'][0]['@value'];
            } else {
                $name = $item['schema:identifier'][0]['@value'];
            }

            $item_data = [
                "id" => ENV('APP_URL') . "/activity/create/" . $signature . ".json",
                "type" => "Create",
                "endTime" => $item['o:created']['@value'],
                "object" => [
                    "id" => ENV('APP_URL') . "/" . $signature . "/manifest.json",
                    "type" => "Manifest",
                    "name" => $name
                ],
                "actor" => [
                    "id" => "https://codeberg.org/PIA/pia-iiif-manifest-host",
                    "type" => "Application"
                ]
            ];

            $data["orderedItems"][] = $item_data;
        }

        if ($actual_page < intval($total_pages)) {
            $data["next"] = [
                "id" => ENV('APP_URL') . "/activity/page-" . ($page + 1) . ".json",
                "type" => "OrderedCollectionPage"
            ];
        }

        if ($actual_page > 1) {
            $data["prev"] = [
                "id" => ENV('APP_URL') . "/activity/page-" . ($page - 1) . ".json",
                "type" => "OrderedCollectionPage"
            ];
        }

        Storage::disk('public')->put($filename, json_encode($data));

        return response()->json($data);
    }
});

Route::get('/activity/create/{signature}.json', function (Request $request, $signature) {

    $filename = 'changes_' . $signature . '.json';

    if (AnnotationController::cached($filename)) {
        return response()->json(json_decode(Storage::disk('public')->get($filename)));
    } else {

        $image_url = "https://participatory-archives.ch/api/items?property[0][property]=1632&property[0][type]=eq&property[0][text]=" . $signature;
        $image = json_decode(file_get_contents($image_url), true)[0];

        $signature = $image['schema:identifier'][0]['@value'];

        if (isset($image['schema:name'])) {
            $name = $image['schema:name'][0]['@value'];
        } else {
            $name = $image['schema:identifier'][0]['@value'];
        }

        $data = [
            "@context" => "http://iiif.io/api/discovery/1/context.json",
            "id" => ENV('APP_URL') . "/activity/create/" . $signature . ".json",
            "type" => "Create",

            "endTime" => $image['o:created']['@value'],

            "object" => [
                "id" => ENV('APP_URL') . "/" . $signature . "/manifest.json",
                "type" => "Manifest",
                "name" => $name
            ],
            "actor" => [
                "id" => "https://codeberg.org/PIA/pia-iiif-manifest-host",
                "type" => "Application"
            ]
        ];

        Storage::disk('public')->put($filename, json_encode($data));

        return response()->json($data);
    }
});
